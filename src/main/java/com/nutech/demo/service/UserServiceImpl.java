package com.nutech.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.SubscriptionBean;
import com.nutech.demo.bean.UserBean;
import com.nutech.demo.bean.UserSubscriptionBean;
import com.nutech.demo.repository.UserRepository;

/**
 * @author Aravindan
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userDao;
	
	private final static String FALLBACKFINDBYID = "fallBackfindById";
	private final static String MOKE = "moke";
	private final static int ID= 1;
	private final static String FALLBACKSUBSCRIBE = "fallBackSubscribe";


	public List<UserBean> getUser() {
		return userDao.getUser();
	}
	
	@HystrixCommand(fallbackMethod= FALLBACKFINDBYID)
	public UserBean findById(int id) {
		return userDao.findById(id);
	}
	public UserBean fallBackfindById(int id)
	{
		UserBean user = new UserBean();
		user.setId(ID);
		user.setUsername(MOKE);
		user.setCountry(MOKE);
		user.setName(MOKE);
		user.setPassword(MOKE);
		return user;
		
	}
	
	public UserBean findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	public void createUser(UserBean user) {
		userDao.addUser(user);
	}

	public void deleteUserById(int id) {
		userDao.delete(id);
	}
	
	@Override
	public UserBean updateUser(UserBean userBean) {
		return userDao.updateUser(userBean);
	}

	@Override
	public UserBean loginUser(LoginBean loginBean) {
		UserBean user = userDao.loginUser(loginBean);
		return user;
	}

	
	@Override
	@HystrixCommand(fallbackMethod=FALLBACKSUBSCRIBE)
	public String subscribe(UserBean user, UserSubscriptionBean bean) {
		SubscriptionBean subBean = userDao.SubcribeUser(user,bean);
		return userDao.userSubcribe(subBean);
		 
	}
	
	public String fallBackSubscribe(UserBean user, UserSubscriptionBean bean)
	{
		return MOKE;
	}

@Override
	@HystrixCommand(fallbackMethod="fallbackGetUserById")
	public UserBean getUserById(int id) {
		return userDao.getUserById(id);
	}		
	public UserBean fallbackGetUserById(int id)
	{
		UserBean bean = new UserBean();
		bean.setId(1);
		bean.setName("mock");
		bean.setUsername("mock");
		bean.setCountry("mock");
		return bean;
	}
}

package com.nutech.demo.service;

import java.util.List;

import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.UserBean;
import com.nutech.demo.bean.UserSubscriptionBean;

/**
 * @author Aravindan
 *
 */
public interface UserService {
	public void createUser(UserBean user);
	public List<UserBean> getUser();
	public UserBean findById(int id);
	public UserBean getUserById(int id);
	public UserBean findByUsername(String username);
	public UserBean loginUser(LoginBean loginBean);
	public UserBean updateUser(UserBean user);
	public void deleteUserById(int id);
	public String subscribe(UserBean user, UserSubscriptionBean bean);
	
}

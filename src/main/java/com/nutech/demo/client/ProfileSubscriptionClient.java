package com.nutech.demo.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nutech.demo.bean.SubscriptionBean;

@FeignClient(name="subscription-service", url="http://localhost:8761")
public interface ProfileSubscriptionClient {
	
	 @RequestMapping(value = "/subscription/{profileId}", method = RequestMethod.GET)
	 public SubscriptionBean getSubscriptionStatus(@PathVariable("profileId") int id);

	/* @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	 public Product getProduct(@PathVariable("id") int productId);*/
	
}
